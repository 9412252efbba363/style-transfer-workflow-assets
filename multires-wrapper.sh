#!/usr/bin/env bash
# Multires script

# USAGE
# ./multires-wrapper.sh [CONTENT IMAGE] [STYLE IMAGES] [OPTIONAL OUTPUT DIRECTORY]
# Option 3 is optional. Specify the path relative to the pwd. If output directory is not specified, it outputs to a folder named after today's date + a sequence number to avoid collisions
# If you have a partial run, ie it crapped out at pass 6, you can pass the output directory from the prior run and it will pick up where it left off.

CONTENT="$1" 
STYLE="$2"
OUTNAME="$3" # Later is initialized to today's date if not set

# This is basically everything that doesn't vary from iteration to iteration, ie what backend, how to configure models, etc
THELINE="th neural_style.lua -gpu 0 -print_iter 1 -save_iter 100 -backend clnn -init image \
		-model_file models/nin_imagenet_conv.caffemodel -proto_file models/train_val.prototxt -content_layers relu1,relu2,relu7 -style_layers relu1,relu2,relu3,relu5,relu7 -learning_rate 1 \
		-style_image "${STYLE}" -content_image "${CONTENT}""

if [[ ${OUTNAME} = "" ]]
then
	for i in $(seq 10000)
	do
		if [[ -d ./"$(date -I)-$i" ]]
		then 
			continue
		elif [[ ! -d ./"$(date -I)-$i" ]]
		then
			OUTNAME="$(date -I)-$i"
			break
		fi
	done
fi

echo "Set output directory to ${OUTNAME}"

if [[ ! -d ${OUTNAME} ]]
then
	echo "Creating output directory ${OUTNAME}..."
	mkdir -vp ./${OUTNAME}
elif [[ -d ${OUTNAME} ]]
	then echo "${OUTNAME} already exists, proceeding to execution..."
fi

if [[ -f ${OUTNAME}/out1.jpg ]]
then
	echo "found output image ${OUTNAME}/out1.jpg from earlier run, skipping pass 1"
else
	false
	while [[ $? -ne 0 ]]
	do 
#		th neural_style.lua -gpu 0 -print_iter 1 -save_iter 100 -backend clnn -init image -model_file models/nin_imagenet_conv.caffemodel -proto_file models/train_val.prototxt -content_layers relu1,relu2,relu7 -style_layers relu1,relu2,relu3,relu5,relu7 -learning_rate 1
		${THELINE} \
			-style_scale 1.0 \
			-tv_weight 0 \
			-content_weight 5e0 \
			-style_weight 2e5 \
			-output_image ${OUTNAME}/out1.jpg \
			-image_size "640" -num_iterations 2000
	done
fi

if [[ -f "${OUTNAME}/out2.jpg" ]]
then
	echo "found output ${OUTNAME}/out2.jpg from prior run, skipping pass 2"
else
	false
	while [[ $? -ne 0 ]]
	do 
		${THELINE} \
			-init_image "${OUTNAME}/out1.jpg" \
			-style_scale 1.0 \
			-tv_weight 0 \
			-content_weight 5e0 \
			-style_weight 2e4 \
			-output_image "${OUTNAME}/out2.jpg" \
			-image_size "768" -num_iterations 1000

	done
fi 	

if [[ -f "${OUTNAME}/out3.jpg" ]]
then
	echo "found output ${OUTNAME}/out3.jpg from prior run, skipping pass 3"
else
	false
	while [[ $? -ne 0 ]]
	do 
		${THELINE} \
			-init_image "${OUTNAME}/out2.jpg" \
			-style_scale 1.0 \
			-tv_weight 0 \
			-content_weight 5e0 \
			-style_weight 2e3 \
			-output_image "${OUTNAME}/out3.jpg" \
			-image_size "1024" -num_iterations 500
	done
fi

if [[ -f "${OUTNAME}/out4.jpg" ]]
then
	echo "found output ${OUTNAME}/out4.jpg from prior run, skipping pass 4"
else
	false
	while [[ $? -ne 0 ]]
	do 
		${THELINE} \
			-init_image "${OUTNAME}/out3.jpg" \
			-style_scale 1.0 \
			-tv_weight 0 \
			-content_weight 5e0 \
			-style_weight 2e2 \
			-output_image "${OUTNAME}/out4.jpg" \
			-image_size "1536" -num_iterations 500
	done
fi

if [[ -f "${OUTNAME}/out5.jpg" ]]
then
	echo "found output ${OUTNAME}/out5.jpg from prior run, skipping pass 5"
else
	false 
	while [[ $? -ne 0 ]]
	do 
		${THELINE} \
			-init_image "${OUTNAME}/out4.jpg" \
			-style_scale 1.0 \
			-tv_weight 0 \
			-content_weight 5e0 \
			-style_weight 2e2 \
			-output_image "${OUTNAME}/out5.jpg" \
			-image_size "2048" -num_iterations 100 -optimizer adam
	done
fi

if [[ -f ${OUTNAME}/out6.jpg ]]
then
	echo "found output ${OUTNAME}/out6.jpg from prior run, skipping pass 6"
else
	false
	while [[ $? -ne 0 ]]
	do 
		${THELINE} \
			-init_image "${OUTNAME}/out5.jpg" \
			-style_scale 1.0 \
			-tv_weight 1e-5 \
			-content_weight 5e0 \
			-style_weight 3e3 \
			-output_image "${OUTNAME}/out6.jpg" \
			-image_size "2560" -num_iterations 100 -optimizer adam
	done
fi

echo "Saved outputs to ${OUTNAME}."